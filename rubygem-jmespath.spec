%global gem_name jmespath
%{?scl:%scl_package rubygem-%{gem_name}}
%{!?scl:%global pkg_name %{name}}

%if 0%{?fedora} && 0%{?fedora} <= 21 || 0%{?rhel} && 0%{?rhel} <= 7
%global use_tests 0
%else
%global use_tests 1
%endif

Name:           %{?scl_prefix}rubygem-%{gem_name}
Version:        1.1.3
Release:        1%{?dist}
Summary:        JMESPath - Ruby Edition

Group:          Development/Languages
License:        ASL 2.0
URL:            http://github.com/trevorrowe/jmespath.rb
Source0:        %{gem_name}-%{version}.gem
Source1:        %{pkg_name}-%{version}-repo.tgz

BuildArch:      noarch
BuildRequires:  %{?scl_prefix}rubygems-devel
%if 0%{?use_tests}
BuildRequires:  %{?scl_prefix}rubygem(rspec) >= 3
BuildRequires:  %{?scl_prefix}rubygem(rspec) < 4
%endif
%if 0%{?fedora} && 0%{?fedora} <= 20 || 0%{?rhel} && 0%{?rhel} <= 7
Requires:       %{?scl_prefix}ruby
Requires:       %{?scl_prefix}ruby(rubygems)
Provides:       %{?scl_prefix}rubygem(%{gem_name}) = %{version}
%{?scl:Requires: %scl_runtime}
%endif

%description
Implements JMESPath for Ruby.


%package doc
Summary:        Documentation for %{pkg_name}
Group:          Documentation
BuildArch:      noarch
Requires:       %{pkg_name} = %{version}-%{release}

%description doc
Documentation for %{pkg_name}.


%prep
%{?scl:scl enable %{scl} "}
gem unpack %{SOURCE0}

%setup -q -D -T -n %{gem_name}-%{version} -a 1

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec
%{?scl:"}


%build

CONFIGURE_ARGS="--with-cflags='%{optflags}' $CONFIGURE_ARGS"
%{?scl:scl enable %{scl} "}
gem build %{gem_name}.gemspec

mkdir -p .%{gem_dir}

gem install \
        -V \
        --local \
        --install-dir .%{gem_dir} \
        --bindir .%{_bindir} \
        --force \
        --ri --rdoc \
        %{gem_name}-%{version}.gem
%{?scl:"}

%install
%{?scl:scl enable %{scl} "}
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

cp -a CHANGELOG.md README.md %{buildroot}%{gem_instdir}/
%{?scl:"}

%check
%if 0%{?use_tests}
cp -pr spec/ ./%{gem_instdir}
pushd .%{gem_instdir}
# simplecov not really needed
sed -i spec/compliance_spec.rb spec/spec_helper.rb -e '/simplecov\|SimpleCov/d'
rspec -Ilib spec
popd
%endif


%files
%dir %{gem_instdir}/
%if 0%{?rhel} && 0%{?rhel} > 6
%license %{gem_instdir}/LICENSE.txt
%endif
%{gem_libdir}/
%{gem_spec}
%exclude %{gem_cache}

%files doc
%if 0%{?rhel} && 0%{?rhel} <= 6
%doc %{gem_instdir}/LICENSE.txt
%endif
%doc %{gem_docdir}/
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.md


%changelog
* Sun Nov 08 2015 František Dvořák <valtri@civ.zcu.cz> - 1.1.3-1
- Updated to 1.1.3
- Removed multi_json dependency in upstream
- Removed patch, all compliance tests OK now

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Thu Jan 29 2015 František Dvořák <valtri@civ.zcu.cz> - 1.0.2-2
- Removed rubygem(simplecov) BR
- Cleanups

* Fri Dec 05 2014 František Dvořák <valtri@civ.zcu.cz> - 1.0.2-1
- Initial package
